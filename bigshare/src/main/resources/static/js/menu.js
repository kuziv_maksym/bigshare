$(document).ready(function () {
        $('.wrapper .content .main .card .colorItem').on('click', function () {
            var imgPath, id;
            id = $(this).attr('data-i-sel');
            imgPath = $(this).attr('data-img-path');
            $('.imgHolder img[data-i-sel='+id+']').fadeOut(500, function f() {
                $('.imgHolder img[data-i-sel='+id+']').attr('src', imgPath).fadeIn(500);
            });
        });


});