$(document).ready(function () {
    $('#colorsSelector .colorItem').on('click', function () {
        var imgPath;
        imgPath = $(this).attr('data-img-path');
        $('.imgHolder img').fadeOut(500, function f() {
            $('.imgHolder img').attr('src', imgPath).fadeIn(500);
        });
    });
});