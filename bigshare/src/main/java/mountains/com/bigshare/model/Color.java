package mountains.com.bigshare.model;

import org.springframework.web.bind.annotation.Mapping;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Color {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String clr;

    private String img;

    public Color() {
    }

    public Color(String clr, String img) {
        this.clr = clr;
        this.img = img;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClr() {
        return clr;
    }


    public void setClr(String color) {
        this.clr = color;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Color color = (Color) object;
        return Objects.equals(id, color.id) &&
                Objects.equals(clr, color.clr) &&
                Objects.equals(img, color.img);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clr, img);
    }

    @Override
    public String toString() {
        return "Color{" +
                "id=" + id +
                ", clr='" + clr + '\'' +
                ", imga='" + img + '\'' +
                '}';
    }
}
