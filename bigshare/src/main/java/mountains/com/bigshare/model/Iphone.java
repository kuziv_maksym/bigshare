package mountains.com.bigshare.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.List;

@Entity
@Table(name = "IPHONE")
public class Iphone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String imga;

    private String name;

    private int price;

    private int memory;

    private String diagonal;

    private String resolution;

    private String camera;

    private int nucleus;

    private String security;

    @OneToMany(targetEntity = Color.class, cascade=CascadeType.ALL)
    @JoinColumn(name="i_id",  referencedColumnName = "id")
    private List<Color> colors = new ArrayList<>();

    public Iphone(String imga, String name, int price, int memory, String diagonal, String resolution, String camera, int nucleus, String security, List<Color> colors) {
        this.imga = imga;
        this.name = name;
        this.price = price;
        this.memory = memory;
        this.diagonal = diagonal;
        this.resolution = resolution;
        this.camera = camera;
        this.nucleus = nucleus;
        this.security = security;
        this.colors = colors;
    }

    public Iphone() {
    }

    public String getImga() {
        return imga;
    }

    public void setImga(String imga) {
        this.imga = imga;
    }

    public List<Color> getColors() {
        return colors;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public String getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(String diagonal) {
        this.diagonal = diagonal;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getCamera() {
        return camera;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public int getNucleus() {
        return nucleus;
    }

    public void setNucleus(int nucleus) {
        this.nucleus = nucleus;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Iphone iphone = (Iphone) object;
        return price == iphone.price &&
                memory == iphone.memory &&
                nucleus == iphone.nucleus &&
                Objects.equals(id, iphone.id) &&
                Objects.equals(imga, iphone.imga) &&
                Objects.equals(name, iphone.name) &&
                Objects.equals(diagonal, iphone.diagonal) &&
                Objects.equals(resolution, iphone.resolution) &&
                Objects.equals(camera, iphone.camera) &&
                Objects.equals(security, iphone.security) &&
                Objects.equals(colors, iphone.colors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, imga, name, price, memory, diagonal, resolution, camera, nucleus, security, colors);
    }

    @Override
    public String toString() {
        return "Iphone{" +
                "id=" + id +
                ", imga='" + imga + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", memory=" + memory +
                ", diagonal='" + diagonal + '\'' +
                ", resolution='" + resolution + '\'' +
                ", camera='" + camera + '\'' +
                ", nucleus=" + nucleus +
                ", security='" + security + '\'' +
                ", colors=" + colors +
                '}';
    }
}
