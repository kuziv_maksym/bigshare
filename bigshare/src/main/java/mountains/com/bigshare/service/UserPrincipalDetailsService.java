package mountains.com.bigshare.service;

import mountains.com.bigshare.model.User;
import mountains.com.bigshare.model.UserPrincipal;
import mountains.com.bigshare.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    public UserPrincipalDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsername(username);
        System.out.println(user);
        UserPrincipal userPrincipal = new UserPrincipal(user);
        return userPrincipal;
    }

    public void saveUser(User user) {
        userRepository.save(user);
    }
}
