package mountains.com.bigshare.service;

import mountains.com.bigshare.model.Iphone;
import mountains.com.bigshare.repository.IphoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class IphoneService {

    @Autowired
    private IphoneRepository iphoneRepository;


    public Iphone getIphone(Integer id) {
        Iphone iphone = iphoneRepository.findIphoneById(id);
        return iphone;
    }

    public List<Iphone> getIphones() {
        return iphoneRepository.findAll();
    }

    public Iphone save(Iphone iphone) {
        return iphoneRepository.save(iphone);
    }
}
