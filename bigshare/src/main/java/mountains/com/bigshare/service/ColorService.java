package mountains.com.bigshare.service;

import mountains.com.bigshare.model.Color;
import mountains.com.bigshare.repository.ColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ColorService {

    @Autowired
    private ColorRepository colorRepository;

    public Color save(Color color) {
        return colorRepository.save(color);
    }


    public List<Color> findAll() {
        return colorRepository.findAll();
    }
}
