package mountains.com.bigshare.repository;

import mountains.com.bigshare.model.Iphone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IphoneRepository extends JpaRepository<Iphone, Integer> {
    Iphone findIphoneById(Integer id);
}
