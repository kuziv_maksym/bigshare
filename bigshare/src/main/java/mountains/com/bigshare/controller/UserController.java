package mountains.com.bigshare.controller;


import mountains.com.bigshare.model.User;
import mountains.com.bigshare.service.UserPrincipalDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserPrincipalDetailsService userPrincipalDetailsService;

    @GetMapping("/new")
    public String getSaveUser() {
        return "user";
    }

    @PostMapping("/new")
    public String saveUser(User user) {
        userPrincipalDetailsService.saveUser(user);
        return "user";
    }
}
