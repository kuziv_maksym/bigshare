package mountains.com.bigshare.controller;


import mountains.com.bigshare.model.Color;
import mountains.com.bigshare.model.Iphone;
import mountains.com.bigshare.service.ColorService;
import mountains.com.bigshare.service.IphoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class EditIphoneController {

    @Autowired
    private IphoneService iphoneService;

    @Autowired
    private ColorService colorService;

    @RequestMapping(value = "/settings/iphone", method = RequestMethod.GET)
    public String getForm(Model model) {
        model.addAttribute("iphone", new Iphone());
        model.addAttribute("color", new Color());
        return "iphone-form";
    }

    @RequestMapping(value = "/settings/iphone", method = RequestMethod.POST)
    public String sendForm(@ModelAttribute("iphone") Iphone iphone,
                           @ModelAttribute("color") Color color,
                           Model model) {
        iphoneService.save(iphone);
        colorService.save(color);
        System.out.println(iphone);
        return "redirect:/settings/iphone";
    }
}
