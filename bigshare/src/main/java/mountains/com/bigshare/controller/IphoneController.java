package mountains.com.bigshare.controller;


import mountains.com.bigshare.model.Color;
import mountains.com.bigshare.model.Iphone;
import mountains.com.bigshare.service.ColorService;
import mountains.com.bigshare.service.IphoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/mountains")
public class IphoneController {

    @Autowired
    private IphoneService iphoneService;

    private static int id_c;

    @Autowired
    private ColorService colorService;

    @GetMapping("/iphone/{id}")
    public String getIphone(@PathVariable Integer id, Model model
    ) {
        Iphone iphone = iphoneService.getIphone(id);
        model.addAttribute("iphone", iphone);
        model.addAttribute("icolor", new Color());
        return "iphone";
    }

    @PostMapping("/iphone/{id}")
    public String setColors(@PathVariable Integer id,
                            @ModelAttribute("icolor") Color icolor) {
        Iphone iphone = iphoneService.getIphone(id);
        List <Color> list = colorService.findAll();
        id_c = 0;
        for (Color color:
             list) {
            if (color.getId() >id) {
                id = color.getId();
            }
        }
        icolor.setId(id+1);
        iphone.getColors().add(icolor);
        iphoneService.save(iphone);
        return "redirect:/mountains/iphone/{id}";
    }

    @GetMapping
    public String getIndex(Model model) {
        model.addAttribute("iphones", iphoneService.getIphones());
        return "index";
    }
}