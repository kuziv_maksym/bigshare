<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="/js/menu.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/css/style.css" href="/css/style.css">
    <link rel="stylesheet" href="/css/main.css" href="/css/main.css">
    <link rel="stylesheet" href="/css/iphone-edit.css" href="/css/iphone-edit.css">
    <meta charset="ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>
<div class="container">
    <form:form action="/settings/iphone" method="post" modelAttribute="iphone">
        <div class="row">
            <div class="col-25">
                <label for="iname">Name</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="iname" name="iname" placeholder="iphone name..." path="name"/>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="iprice">Price</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="iprice" name="iprice" placeholder="price..." path="price"/>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="idiagonal">Diagonal</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="idiagonal" name="idiagonal" placeholder="diagonal..." path="diagonal"/>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="iresolution">Resolution</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="iresolution" name="iresolution" placeholder="resolution..." path="resolution"/>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="imemory">Memory</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="imemory" name="imemory" placeholder="memory..." path="memory"/>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="icamera">Camera</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="icamera" name="icamera" placeholder="camera..." path="camera"/>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="inucleus">Nucleus</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="inucleus" name="inucleus" placeholder="nucleus..." path="nucleus"/>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="isecurity">Security</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="isecurity" name="isecurity" placeholder="security..." path="security"/>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="iimga">Image</label>
            </div>
            <div class="col-75">
                <form:input type="text" id="iimga" name="iimga" placeholder="image..." path="imga"/>
            </div>
        </div>
        <div class="row">
            <form:button value="Submit" type="submit">Submit</form:button>
        </div>
    </form:form>


</div>
</body>
</html>