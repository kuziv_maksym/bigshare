<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="/js/main.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/css/main.css" href="/css/main.css">
    <meta charset="ISO-8859-1">
    <title>Iphone</title>
    <link href="${contextPath}/resource/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div id="imgHoler" class="imgHolder">
    <img id="img-cl" class="img-cl" src="${iphone.imga}" alt="Nonephoto"/>
    <div id="colorsSelector" class="colorsSelector">
        <h3>${iphone.name}</h3>
        <h4>${iphone.price}</h4>
        ${iphone.memory}
        ${iphone.diagonal}
        ${iphone.resolution}
        ${iphone.camera}
        ${iphone.nucleus}
        ${iphone.security}
        <c:forEach var="color" items="${iphone.colors}">
        <div class="colorItem"  data-img-path="${color.img}"
             style="background-color:${color.clr}">
        </div>
        </c:forEach>

        <form:form action="/mountains/iphone/${iphone.id}" method="post" modelAttribute="icolor">
            <div class="row">
                <div class="col-25">
                    <label for="iclr">Color</label>
                </div>
                <div class="col-75">
                    <form:input type="text" id="iclr" name="iclr" placeholder="color..." path="clr"/>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="iimg">Image</label>
                </div>
                <div class="col-75">
                    <form:input type="text" id="iimg" name="iimg" placeholder="image..." path="img"/>
                </div>
            </div>
            <div class="row">
                <form:button value="Submit" type="submit">Submit</form:button>
            </div>
        </form:form>
    </div>
</div>
</body>
</html>