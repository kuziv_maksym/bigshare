<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="/js/menu.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/css/style.css" href="/css/style.css">
    <link rel="stylesheet" href="/css/main.css" href="/css/main.css">
    <meta charset="ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <ul>
            <li><a class="active" href="#home">Mountains</a></li>
            <li><a href="#news">News</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a href="#about">About</a></li>
            <li style="float:right"><a class="active" href="#about">About</a></li>
        </ul>
    </div>
    <div class="content">
        <div class="main">
            <c:forEach var="iphone" items="${iphones}">
            <div class="card">
                <div class="imgHolder">
                    <img data-i-sel="${iphone.id}" src="${iphone.imga}" alt="none">
                </div>
                <hr>
                <div class="colorSelector">
                    <c:forEach var="color" items="${iphone.colors}">
                    <div class="colorItem" id="colorItem"
                         data-i-sel="${iphone.id}"
                         data-img-path="${color.img}"
                         style="background-color:${color.clr}">
                    </div>
                    </c:forEach>
                </div>
                <div class="inform">
                    <span id = "mobile">Mobile phone</span>
                    <h3 id = "name">${iphone.name} ${iphone.memory} Gb</h3>


                </div>
                <div class="pricePhone"><h4 id = "price">${iphone.price}</h4><h5 id="priceH"> ГРН</h5></div>
                <div class="link-s"><a class="link" href="/mountains/iphone/${iphone.id}"><span id="text-a">Detail</span></a></div>
            </div>
            </c:forEach>
        </div>
    </div>
    <div class="footer"></div>
</div>
</body>
</html>